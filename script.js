 let courses = [
 		{
 			id: "Python101",
 			name: "Python",
 			description: "study python",
 			price: 1000,
 			isActive: true
 		},
 		{
 			id: "Javascript101",
 			name: "Javascript",
 			description: "study javascript",
 			price: 800,
 			isActive: true
 		},
 		{
 			id: "Visual Basics 101",
 			name: "Visual Basics",
 			description: "study visual basics",
 			price: 400,
 			isActive: true
 		},
 		{
 			id: "HTML101",
 			name: "HTML",
 			description: "study HTML",
 			price: 450,
 			isActive: true
 		},
 		{
 			id: "CSS101",
 			name: "CSS",
 			description: "study CSS",
 			price: 450,
 			isActive: true
 		},
 ]

// ADD COURSE
// CREATE

	 const addCourse = (id1,name1,description1,price1,isActive1) => courses.push({
	 	id: id1,
	 	name: name1,
	 	description: description1,
	 	price: price1,
	 	isActive: isActive1	
	 })

console.log(courses)

// addCourse("Java101","Java","learn Java",1500,true)

// ARCHIVE COURSE
// UPDATE

	const archiveCourse = (index) =>{
		 if (courses[index].isActive == true){
				courses[index].isActive = false;

		} else {
			courses[index].isActive = true
		}

		return courses[index];
	}
	

// DELETE COURSE
// DELETE
	const deleteCourse = () => courses.pop();


// GET SINGLE COURSE 
// GET SPECIFIC COURSE BY ID

	const getSingleCourse = (id1) => {
		let foundCourse = courses.find(function(user){
			// if(courses.id == user){
				// console.log(courses);
			return user.id === id1;
			});
			console.log(foundCourse);
		} 

// RETRIEVE/READ
	
	const getAllCourse = () => console.log(courses)



// STRETCH GOALS
	

// SHOW COURSES THAT are isActive == true 
	const activeCourse = () => {
			let newCourse = courses.filter(function(element){
				if(element.isActive== true){
				return element;
				}
			});
			console.log(newCourse)
	}

	const archiveCourse1 = (name1)
